#include <stdio.h>
#include <iostream>

#include <opencv2/opencv.hpp>

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 6
#define SQUARE_SIZE (15.0/16.0)

namespace {
void print_usage(const char* name) {
  printf("%s: INPUT_IMAGES CAMERA_CALIB\n", name);
  printf("INPUT_IMAGES: XML file with images.\n");
  printf("CAMERA_CALIB: XML file with camera calibration matrices.\n");
}

}  // namespace

int main(int argc, char** argv) {
  if (argc != 3) {
    print_usage(argv[0]);
    return -1;
  }

  std::string input_xml_filename(argv[1]);

  printf("Reading images from xml: %s\n", input_xml_filename.c_str());

  cv::FileStorage fs(input_xml_filename, cv::FileStorage::READ);
  // Read in image filenames.
  cv::FileNode fn = fs["calib_images"];
  if (fn.type() != cv::FileNode::SEQ) {
    fprintf(stderr, "calib_image is a not a sequence in xml!!\n");
    return -1;
  }

  // Open XML
  std::vector<std::string> image_filenames;
  for (cv::FileNodeIterator it = fn.begin(); it != fn.end(); ++it) {
    image_filenames.push_back(*it);
  }

  // Be verbose about what we read in.
  printf("Reading ");
  for (const std::string& filename : image_filenames) {
    printf("%s, ", filename.c_str());
  }
  printf("\n");

  // Read in all images
  std::vector<cv::Mat> input_images;
  for (const std::string& filename : image_filenames) {
    // Read grayscale images in on purpose.
    input_images.push_back(cv::imread(filename));
  }

  // Read in camera matrices
  std::string camera_xml_filename(argv[2]);
  printf("Reading camera matrices from %s.\n", camera_xml_filename.c_str());

  cv::FileStorage fs_camera(camera_xml_filename, cv::FileStorage::READ);
  cv::Mat cameraMatrix, distCoef;
  fs_camera["cameraMatrix"] >> cameraMatrix;
  fs_camera["distCoef"] >> distCoef;

  cameraMatrix = 2 * cameraMatrix;
  cameraMatrix.at<double>(2, 2) = 1;

  std::cout << cameraMatrix << std::endl;

  cv::namedWindow("Original");
  cv::namedWindow("Undistorted Opencv");
  cv::Mat undistorted_image;
  for (size_t i = 0; i < input_images.size(); ++i) {
    imshow("Original", input_images[i]);

    cv::undistort(input_images[i], undistorted_image, cameraMatrix, distCoef);
    imshow("Undistorted Opencv", undistorted_image);
    cv::waitKey(-1);
  }

  // Now loop and grab points to undistort for example. (-1, -1) for exit.
  printf("Now grabbing points to undistort (-1, -1) for exit.\n");
  int x, y;
  while (std::cin >> x >> y) {
    if (x < 0 || y < 0) {
      break;
    }
    std::vector<cv::Point2f> input_points, undistorted_points;
    input_points.push_back(cv::Point2f(x, y));
    cv::undistortPoints(input_points, undistorted_points, cameraMatrix, distCoef, cv::noArray(), cameraMatrix);
    printf("Input (%d, %d) maps to (%f, %f).\n", x, y, undistorted_points[0].x, undistorted_points[0].y);
  }

}
