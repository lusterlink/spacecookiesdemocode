#include <stdio.h>

#include <iostream>
#include <string>
#include <vector>

#include <opencv2/opencv.hpp>

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 6
#define SQUARE_SIZE (15.0/16.0)

namespace {

void print_usage(const char* name) {
  printf("%s: INPUT_IMAGES CALIB_DUMP\n", name);
  printf("INPUT_IMAGES: XML file with images.\n");
  printf("CALIB_DUMP: XML file to dump calib matrices.\n");
}

}  // namespace

int main(int argc, char** argv) {
  if (argc != 3) {
    print_usage(argv[0]); 
    return -1;
  }

  std::string input_xml_filename(argv[1]);

  printf("Reading images from xml: %s\n", input_xml_filename.c_str());

  // Open XML
  cv::FileStorage fs(input_xml_filename, cv::FileStorage::READ);
  
  // Read in image filenames.
  cv::FileNode fn = fs["calib_images"];
  if (fn.type() != cv::FileNode::SEQ) {
    fprintf(stderr, "calib_image is a not a sequence in xml!!\n");
    return -1;
  }

  std::vector<std::string> image_filenames;
  for (cv::FileNodeIterator it = fn.begin(); it != fn.end(); ++it) {
    image_filenames.push_back(*it);
  }

  // Be verbose about what we read in.
  printf("Reading ");
  for (const std::string& filename : image_filenames) {
    printf("%s, ", filename.c_str());
  }
  printf("\n");

  // Read in all images
  std::vector<cv::Mat> input_images;
  for (const std::string& filename : image_filenames) {
    // Read grayscale images in on purpose.
    input_images.push_back(cv::imread(filename));
  }
  
  // For each image, find chessboard corners and display.
  cv::namedWindow("Chessboard Corners");
  cv::Mat output_chessboard_drawn;
  cv::Mat image;
  std::vector<std::vector<cv::Point2f> > image_corners;
  for (size_t i = 0; i < input_images.size(); ++i) {
    cvtColor(input_images[i], image, CV_RGB2GRAY);
    std::vector<cv::Point2f> single_image_corners;
    bool found =
      cv::findChessboardCorners(image, cv::Size(BOARD_WIDTH, BOARD_HEIGHT),
          single_image_corners);
  
    image.copyTo(output_chessboard_drawn);
    if (found) {
      // Refine corner locations to improve calibration.
      // We use the entire 22x22 window with term criteria of both max
      // iterations and desired accuracy.
      cv::cornerSubPix(image, single_image_corners, cv::Size(3, 3),
          cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::COUNT +
            cv::TermCriteria::EPS, 30, 0.01));

      image_corners.push_back(single_image_corners);
    } else {
      fprintf(stderr, "Chessboard corners not found for %s!!!!\n",
          image_filenames[i].c_str());
    }

    cv::drawChessboardCorners(output_chessboard_drawn,
        cv::Size(BOARD_WIDTH, BOARD_HEIGHT),
        single_image_corners, found);
    imshow("Chessboard Corners", output_chessboard_drawn);
    cv::waitKey(-1);
  }

  // This is vector of the coordinates of the interior corners of a chessboard
  // ordered row-major.
  std::vector<cv::Point3f> chessboard_corners;
  for (int i = 0; i < BOARD_HEIGHT; ++i) {
    for (int j = 0; j < BOARD_WIDTH; ++j) {
      chessboard_corners.push_back(
          cv::Point3f(j * SQUARE_SIZE, i * SQUARE_SIZE, 0));
    }
  }

  // Construct the object points for each image.  Since we are using the
  // CHESSBOARD, keep all the same.
  std::vector<std::vector<cv::Point3f> > object_points(image_filenames.size(),
      chessboard_corners);

  // Now actually call calibrate
  cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
  cv::Mat distCoef = cv::Mat::zeros(8, 1, CV_64F);
  std::vector<cv::Mat>  rvecs;
  std::vector<std::vector<double> > tvecs;
  double final_reproj_error = cv::calibrateCamera(object_points, image_corners,
      cv::Size(input_images[0].rows, input_images[0].cols), cameraMatrix,
      distCoef, rvecs, tvecs, 0, cv::TermCriteria(cv::TermCriteria::COUNT +
            cv::TermCriteria::EPS, 30, 0.001));
  printf("Final reproj error: %f\n", final_reproj_error);

  std::cout << "Camera matrix: " << cameraMatrix << std::endl;
  std::cout << "Dis coef: " << distCoef << std::endl;

  // Set up the outer of the inner corners for redrawing.
  std::vector<cv::Point3f> chessboard_outer_corners;
  chessboard_outer_corners.push_back({0, 0, 0});
  chessboard_outer_corners.push_back({SQUARE_SIZE * (BOARD_WIDTH - 1), 0, 0});
  chessboard_outer_corners.push_back(
      {SQUARE_SIZE * (BOARD_WIDTH - 1), SQUARE_SIZE * (BOARD_HEIGHT - 1), 0});
  chessboard_outer_corners.push_back({0, SQUARE_SIZE * (BOARD_HEIGHT - 1), 0});

  // Undistort.
  cv::Mat undistorted_image, upscaled_image, upscaled_orig;
  cv::namedWindow("Original");
  std::vector<cv::Point2f> outer_inner_projected;
  std::vector<cv::Point2f> outer_inner_undistorted;
  for (size_t i = 0; i < input_images.size(); ++i) {
    const cv::Mat& image = input_images[i];
    cv::undistort(image, undistorted_image, cameraMatrix, distCoef);
 
    // Project via rvecs and tvecs.
    cv::projectPoints(chessboard_outer_corners, rvecs[i], tvecs[i],
        cameraMatrix, distCoef, outer_inner_projected);

    // Now draw 4 lines. On the input_images.
    for (size_t j = 0; j < outer_inner_projected.size() - 1; ++j) {
      cv::line(input_images[i], outer_inner_projected[j],
          outer_inner_projected[j+1], CV_RGB(255, 0, 0));
    }
    cv::line(input_images[i],
        outer_inner_projected[outer_inner_projected.size() - 1],
        outer_inner_projected[0], CV_RGB(255, 0, 0));

    // Now draw the lines on the undistorted image.
    cv::undistortPoints(outer_inner_projected, outer_inner_undistorted,
        cameraMatrix, distCoef, cv::noArray(), cameraMatrix);
    for (size_t j = 0; j < outer_inner_undistorted.size() - 1; ++j) {
      cv::line(undistorted_image, outer_inner_undistorted[j],
          outer_inner_undistorted[j+1], CV_RGB(0, 255, 0));
    }
    cv::line(undistorted_image,
        outer_inner_undistorted[outer_inner_undistorted.size() - 1],
        outer_inner_undistorted[0], CV_RGB(0, 255, 0));

    cv::resize(undistorted_image, upscaled_image, cv::Size(640, 480));
    cv::resize(image, upscaled_orig, cv::Size(640, 480));
    imshow("Original", upscaled_orig);
    imshow("Chessboard Corners", upscaled_image);
    cv::waitKey(-1);
  }

  // Dump camera calibration to disk
  std::string output_xml_filename(argv[2]);
  printf("Dump camera calibration to disk at: %s.",
      output_xml_filename.c_str());
  cv::FileStorage fs_out(output_xml_filename, cv::FileStorage::WRITE);

  fs_out << "cameraMatrix" << cameraMatrix;
  fs_out << "distCoef" << distCoef;
}

