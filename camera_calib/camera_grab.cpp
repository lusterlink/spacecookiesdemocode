#include <stdio.h>

#include <vector>

#include <opencv2/opencv.hpp>


namespace {

void print_usage(const char* name) {
  printf("%s: OUTPUT_IMAGES OUTPUT_DIR\n", name);
  printf("OUTPUT_IMAGES: XML to ouput the image names to.\n");
  printf("OUTPUT_DIR: directory to dump the images to.\n");
}

}  // namespace

int main(int argc, char** argv) {
  if (argc != 3) {
    print_usage(argv[0]);
    return -1;
  }

  std::string output_xml_filename(argv[1]);
  std::string output_images_prefix(argv[2]);

  printf("Dumping xml to %s.\n", output_xml_filename.c_str());
  printf("Dumping images with prefix %s.\n", output_images_prefix.c_str());

  // Open XML
  cv::FileStorage fs(output_xml_filename, cv::FileStorage::WRITE);

  // Open default camera
  cv::VideoCapture cap("http://10.18.68.11/mjpg/video.mjpg");

  // Exit if open failed.
  if (!cap.isOpened()) {
    fprintf(stderr, "Unable to open camera!\n");
    return -1;
  }

  cv::namedWindow("VideoFeed", 0);
  std::vector<cv::Mat> frames;

  while (true) {
    cv::Mat frame;
    cap >> frame;

    cv::imshow("VideoFeed", frame);
    
    // Wait 100ms for a key. 
    int key_code = cv::waitKey(100);

    if (key_code == 'q') {
      // If 'q', quit.
      break;
    } else if (key_code == ' ') {
      // If ' ', grab frame.
      frames.push_back(frame);
      printf("Grabbed frame %lu.\n", frames.size());
    }
  }

  // Now dump frames and xml.
  fs << "calib_images" << "[";
  for (size_t i = 0; i < frames.size(); ++i) {
    char buffer[256];
    sprintf(buffer, "%s/image_%lu.jpg", output_images_prefix.c_str(), i);
    if (!cv::imwrite(buffer, frames[i])) {
      printf("Error dumping frame %lu.\n", i);
    } else {
      fs << buffer;
    }
  }
  fs << "]";

  return 0;
}
