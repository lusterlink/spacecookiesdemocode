#include <stdio.h>
#include <iostream>

#include <opencv2/opencv.hpp>

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 6
#define SQUARE_SIZE (15.0/16.0)

namespace {
void print_usage(const char* name) {
  printf("%s: CAMERA_CALIB\n", name);
  printf("CAMERA_CALIB: XML file with camera calibration matrices.\n");
}

}  // namespace

int main(int argc, char** argv) {
  if (argc != 2) {
    print_usage(argv[0]);
    return -1;
  }

  // Read in camera matrices
  std::string camera_xml_filename(argv[1]);
  printf("Reading camera matrices from %s.\n", camera_xml_filename.c_str());

  cv::FileStorage fs_camera(camera_xml_filename, cv::FileStorage::READ);
  cv::Mat cameraMatrix, distCoef;
  fs_camera["cameraMatrix"] >> cameraMatrix;
  fs_camera["distCoef"] >> distCoef;

  std::cout << cameraMatrix << std::endl;

  cv::Mat frame = cv::imread("greengreengreen.jpg");

  // This is the sized 4 vector of the corners of the target.
  std::vector<cv::Point2f> image_corners= {{458.33, 355.976},{537.99, 351.809},{463.083, 402.525},{542.286, 394.601}};
  std::vector<cv::Point3f> target_corners = {{0, 0, 0}, {12, 0, 0}, {0, 20, 0}, {12, 20, 0}};

  cv::Mat rvec, tvec;
  cv::namedWindow("Results");

  for (size_t j = 0;j < image_corners.size() - 1; ++j) {
    cv::line(frame, image_corners[j], image_corners[j+1], CV_RGB(255, 0, 0));
  }
  cv::line(frame, image_corners[image_corners.size() - 1],
      image_corners[0], CV_RGB(255, 0, 0));



  imshow("Results", frame);
  cv::waitKey(-1);
  if (cv::solvePnP(target_corners, image_corners, cameraMatrix, distCoef, 
        rvec, tvec)) {
    std::vector<cv::Point2f> projected_points;
    cv::projectPoints(target_corners, rvec, tvec, cameraMatrix, distCoef, projected_points);
    for (size_t j = 0;j < projected_points.size() - 1; ++j) {
      cv::line(frame, projected_points[j], projected_points[j+1], CV_RGB(0, 255, 0));
    }
    cv::line(frame, projected_points[projected_points.size() - 1],
        projected_points[0], CV_RGB(0, 255, 0));

    std::cout << "tvec" << tvec << std::endl;
    std::cout << "rvec" << rvec << std::endl;

    imshow("Results", frame);
    cv::waitKey(-1);
  } else {
    printf("Unsucessful solve.");
  }
}
