#!/usr/bin/python

import Blue_Alliance_API
import numpy as np
import os
from os import listdir
from os.path import isfile, join
from sets import Set
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
        help="write result to FILE", metavar="FILE")
parser.add_option("-r", "--recompute", dest="recompute",
        help="recompute results", default=False)
parser.add_option("-s", "--score", dest="score",
        help="type of score (raw_score[default], teloep_crossing, teleop_boulder_high, teleop_boulder_low,auto_boulder_low)", default='raw_score')

(options, args) = parser.parse_args()

if options.recompute:
    print 'Forcing recompute'
elif options.filename is not None and os.path.isfile(options.filename):
    print 'Skipping %s since have on disk.' % options.filename
    exit()

def get_auto_boulder_low(match, alliance):
    return match['score_breakdown'][alliance]['autoBouldersLow']

def get_auto_boulder_high(match, alliance):
    return match['score_breakdown'][alliance]['autoBouldersHigh']

def get_teleop_boulder_high(match, alliance):
    return match['score_breakdown'][alliance]['teleopBouldersHigh']

def get_teleop_boulder_low(match, alliance):
    return match['score_breakdown'][alliance]['teleopBouldersLow']

def get_raw_score(match, alliance):
    return match['alliances'][alliance]['score']

def get_teleop_crossing(match, alliance):
    return match['score_breakdown'][alliance]['teleopCrossingPoints']

def get_lowbar_crossing(match, alliance):
    return match['score_breakdown'][alliance]['position1crossings']

def get_defense_cross(defense_name, match, alliance):
    # Find if this defense was played.
    for i in xrange(2, 6):
        if match['score_breakdown'][alliance]['position%s' % i][2:] == defense_name:
            return match['score_breakdown'][alliance]['position%scrossings' % i]
    # If not, ignore this row
    return -1

def get_auto_defense_score(match, alliance):
    return match['score_breakdown'][alliance]['autoReachPoints'] +\
           match['score_breakdown'][alliance]['autoCrossingPoints']

def get_auto_score(match, alliance):
    return match['score_breakdown'][alliance]['autoPoints']

def get_teleop_boulders_total(match, alliance):
    return get_teleop_boulder_low(match, alliance) + get_teleop_boulder_high(match, alliance)

def get_scale_points(match, alliance):
    return match['score_breakdown'][alliance]['teleopScalePoints']

def get_total_boulders(match, alliance):
    return get_teleop_boulder_low(match, alliance) + get_teleop_boulder_high(match, alliance) +\
           get_auto_boulder_high(match, alliance) + get_auto_boulder_low(match, alliance)


def get_score(match, alliance):
    try:
        if options.score == 'raw_score':
            return get_raw_score(match, alliance)
        elif options.score == 'teleop_crossing':
            return get_teleop_crossing(match, alliance)
        elif options.score == 'teleop_boulder_high':
            return get_teleop_boulder_high(match, alliance)
        elif options.score == 'teleop_boulder_low':
            return get_teleop_boulder_low(match, alliance)
        elif options.score == 'auto_boulder_high':
            return get_auto_boulder_high(match, alliance)
        elif options.score == 'auto_boulder_low':
            return get_auto_boulder_low(match, alliance)
        elif options.score == 'lowbar_crossing':
            return get_lowbar_crossing(match, alliance)
        elif options.score[0:7] == 'defense':
            return get_defense_cross(options.score[8:], match, alliance)
        elif options.score == 'auto_defense':
            return get_auto_defense_score(match, alliance)
        elif options.score == 'auto_score':
            return get_auto_score(match, alliance)
        elif options.score == 'teleop_boulders':
            return get_teleop_boulders_total(match, alliance)
        elif options.score == 'scale_points':
            return get_scale_points(match, alliance)
        elif options.score == 'total_boulders':
            return get_total_boulders(match, alliance)
    except TypeError:
        print match
        return NoneType

np.set_printoptions(threshold=np.nan)

interesting_files = [[f, join('events/', f)] for f in listdir('events/') if isfile(join('events/', f))]

team_numbers_set = Set([])
events = {}
qm_matches = {}

num_matches = 0

print 'Loading data.'
# First load all events and populate set of team numbers
for event_code, file_name in interesting_files:
   # if event_code != '2016casj' and event_code != '2016cada' and event_code != '2016txho':
        #continue
    events[event_code] = Blue_Alliance_API.load_database(file_name)

    team_numbers_set.update(map(lambda x: int(x), events[event_code].rankings[:,1]))

    qm_matches[event_code] = [x for x in events[event_code].raw_matches if x['comp_level'] == 'qm' and x['alliances']['red']['score'] != -1]

    num_matches = num_matches + len(qm_matches[event_code])

# Now sort
sorted_team_list = sorted([x for x in team_numbers_set])
team_index_map = dict(zip(sorted_team_list, range(len(sorted_team_list))))

print 'Computing for %d matches and %d teams.' % (num_matches, len(sorted_team_list))
score = np.zeros((num_matches*2, 1), np.int32)
match_matrix = np.zeros((num_matches*2, len(sorted_team_list)), np.int32)

match_offset = 0
delete_list = []
for event_code in qm_matches:
    max_match_num = -1
    for match in qm_matches[event_code]:
        match_num = match['match_number'] - 1
        # Fill in for red, this is row one
        red_score = get_score(match, 'red')
        if red_score != -1:
            for team_string in match['alliances']['red']['teams']:
                team_number = int(team_string[3:])
                match_matrix[2*match_num + 2*match_offset, team_index_map[team_number]] = 1

            score[2*match_num + 2*match_offset] = red_score
        else:
            delete_list.append(2*match_num + 2*match_offset)

        # Fill in for blue, this is row two
        blue_score = get_score(match, 'blue')
        if blue_score != -1:
            for team_string in match['alliances']['blue']['teams']:
                team_number = int(team_string[3:])
                match_matrix[2*match_num+1 + 2*match_offset, team_index_map[team_number]] = 1

            score[2*match_num+1 + 2*match_offset] = blue_score
        else:
            delete_list.append(2*match_num+1 + 2*match_offset)

        if match_num > max_match_num:
            max_match_num = match_num
    match_offset = match_offset + max_match_num + 1

print 'Removing %d entries.' % len(delete_list)
match_matrix = np.delete(match_matrix, delete_list, 0)
score = np.delete(score, delete_list, 0)

print 'Loaded results performing Least Squares.'
# Results
#results = np.dot(np.dot(np.linalg.inv(np.dot(match_matrix.T, match_matrix)), match_matrix.T), score)
results = np.linalg.lstsq(match_matrix, score)

residuals = np.dot(match_matrix,results[0]) - score
print np.linalg.norm(residuals)

sorted_associated_results=sorted(zip(sorted_team_list, map(lambda x: x[0], results[0].tolist())), key=lambda x: x[1], reverse=True)
sorted_associated_results = [(i, x[0], x[1]) for i, x in enumerate(sorted_associated_results)]

if options.filename is not None:
    np.savetxt(options.filename, sorted_associated_results, delimiter=",", fmt='%.3f')
else:
    print sorted_associated_results
