#!/usr/bin/python

import csv
import numpy as np
from os import listdir
from os.path import isfile, join
from optparse import OptionParser
from sets import Set

import os

parser = OptionParser()
parser.add_option("-e", "--event", dest="event_code",
        help="event to filter against", default='2016cmp')
parser.add_option("-i", "--infile", dest="in_filename",
        help="file to filter", metavar="FILE")
parser.add_option("-f", "--file", dest="out_filename",
        help="file to filter to", metavar="FILE")
parser.add_option("-r", "--recompute", dest="recompute",
        help="recompute results", default=False)

(options, args) = parser.parse_args()

if options.in_filename is None:
    print 'Need in filename.'
    exit()

if options.recompute:
    print 'Forcing recompute'
elif options.out_filename is not None and os.path.isfile(options.out_filename):
    print 'Skipping %s since have on disk.' % options.out_filename
    exit()


team_set = Set()

if options.event_code == '2016cmp':
    interesting_files = [[f, join('divisions/', f)] for f in listdir('divisions/') if isfile(join('divisions/', f))]
    for f, filename in interesting_files:
        print 'Opening %s.' % f
        reader=csv.reader(open(filename,"rb"),delimiter=' ')
        teams = map(lambda x: x[0], list(reader))
        print 'Found %d teams.' % len(teams)
        team_set.update(teams)
else:
    filename = 'divisions/%s' % options.event_code
    print 'Opening %s' % filename
    reader=csv.reader(open(filename,"rb"),delimiter=' ')
    teams = map(lambda x: x[0], list(reader))
    team_set.update(teams)

team_set = Set(map(lambda x: int(x), team_set))
print 'Total %d teams.' % len(team_set)

# Read in numpy array from csv in.
team_rankings = np.genfromtxt(options.in_filename, delimiter=',')

# And filter
if team_rankings[0][0] == 0:
    filtered_teams = filter(lambda x: x[1] in team_set, team_rankings)
    sorted_filtered_teams = [x[1:] for x in filtered_teams]
else:
    filtered_teams = filter(lambda x: x[0] in team_set, team_rankings)
    sorted_filtered_teams = filtered_teams

np.set_printoptions(threshold=np.nan)

sorted_filtered_teams = np.insert(sorted_filtered_teams, 0, range(len(filtered_teams)), 1)

if options.out_filename is not None:
    np.savetxt(options.out_filename, sorted_filtered_teams, delimiter=",", fmt='%.3f')
else:
    print sorted_filtered_teams
