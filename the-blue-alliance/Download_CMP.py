import json
import os
import numpy as np
import urllib2 as REQUEST
from sets import Set
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-e", "--event", dest="event_code",
        help="event to filter against", default='2016cmp')
parser.add_option("-i", "--infile", dest="in_filename",
        help="file to filter", metavar="FILE")
parser.add_option("-f", "--file", dest="out_filename",
        help="file to filter to", metavar="FILE")
parser.add_option("-r", "--recompute", dest="recompute",
        help="recompute results", default=False)
(options, args) = parser.parse_args()

if options.in_filename is None:
    print 'Need in filename.'
    exit()

if options.recompute:
    print 'Forcing recompute'
elif options.out_filename is not None and os.path.isfile(options.out_filename):
    print 'Skipping %s since have on disk.' % options.out_filename
    exit()

URL = 'http://www.thebluealliance.com/api/v2/'
HEADER = 'X-TBA-App-Id'
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0'
VALUE = 'team1868:test:1.0'

def _pull_request(request_code):
    request = REQUEST.Request(request_code)
    request.add_header(HEADER, VALUE)
    request.add_header('User-Agent', USER_AGENT)
    response = REQUEST.urlopen(request)
    response = json.loads(response.read().decode("utf-8"))
    return response

print 'Filtering from %s.' % options.event_code
raw_teams = _pull_request(URL + ('event/%s/teams' % options.event_code))

# Extra team numbers
cmp_team_numbers = Set([x['team_number'] for x in raw_teams])

# Read in numpy array from csv in.
team_rankings = np.genfromtxt(options.in_filename, delimiter=',')

# And filter
if team_rankings[0][0] == 0:
    filtered_teams = filter(lambda x: x[1] in cmp_team_numbers, team_rankings)
    sorted_filtered_teams = [x[1:] for x in filtered_teams]
else:
    filtered_teams = filter(lambda x: x[0] in cmp_team_numbers, team_rankings)
    sorted_filtered_teams = filtered_teams

sorted_filtered_teams = np.insert(sorted_filtered_teams, 0, range(len(filtered_teams)), 1)

if options.out_filename is not None:
    np.savetxt(options.out_filename, sorted_filtered_teams, delimiter=",", fmt='%.3f')
else:
    print sorted_filtered_teams
