#!/usr/bin/python

import Blue_Alliance_API
import numpy as np
import os
from os import listdir
from os.path import isfile, join
from sets import Set
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
        help="write result to FILE", metavar="FILE")
parser.add_option("-r", "--recompute", dest="recompute",
        help="recompute results", default=False)
parser.add_option("-s", "--score", dest="score",
        help="type of score (defense)", default='raw_score')

(options, args) = parser.parse_args()

if options.recompute:
    print 'Forcing recompute'
elif options.filename is not None and os.path.isfile(options.filename):
    print 'Skipping %s since have on disk.' % options.filename
    exit()


def get_defense_crossings(defense, match, alliance):
    for i in range(2, 6):
        if match['score_breakdown'][alliance]['position%s' % i][2:] == defense:
            return match['score_breakdown'][alliance]['position%scrossings' % i]
    return -1

def get_teleop_breachs(match, alliance):
    if match['score_breakdown'][alliance]['teleopDefensesBreached']:
        return 1
    return 0

def get_teleop_capture(match, alliance):
    if match['score_breakdown'][alliance]['teleopTowerCaptured']:
        return 1
    return 0

def get_score(match, alliance, team_number):
    try:
        if options.score[0:7] == 'defense':
            return get_defense_crossings(options.score[8:], match, alliance)
        elif options.score == 'breach':
            return get_teleop_breachs(match, alliance)
        elif options.score == 'capture':
            return get_teleop_capture(match, alliance)
    except TypeError:
        print match
        return NoneType


np.set_printoptions(threshold=np.nan)
interesting_files = [[f, join('events/', f)] for f in listdir('events/') if isfile(join('events/', f))]

team_numbers_set = Set([])
events = {}
qm_matches = {}

num_matches = 0

print 'Loading data.'

# First load all events and populate set of team numbers
for event_code, file_name in interesting_files:
#    if event_code != '2016casj' and event_code != '2016cada' and event_code != '2016txho':
#        continue
    events[event_code] = Blue_Alliance_API.load_database(file_name)

    team_numbers_set.update(map(lambda x: int(x), events[event_code].rankings[:,1]))

    qm_matches[event_code] = [x for x in events[event_code].raw_matches if x['comp_level'] == 'qm' and x['alliances']['red']['score'] != -1]

    num_matches = num_matches + len(qm_matches[event_code])

# Now sort
sorted_team_list = sorted([x for x in team_numbers_set])
team_index_map = dict(zip(sorted_team_list, range(len(sorted_team_list))))

score = np.zeros((len(sorted_team_list), 1), np.int32)
num_matches = np.zeros((len(sorted_team_list), 1), np.int32)

for event_code in qm_matches:
    for match in qm_matches[event_code]:
        for team_string in match['alliances']['red']['teams']:
            team_number = int(team_string[3:])
            team_score = get_score(match, 'red', team_number)
            if team_score != -1:
                score[team_index_map[team_number]] = score[team_index_map[team_number]] + team_score
                num_matches[team_index_map[team_number]] = num_matches[team_index_map[team_number]] + 1

        for team_string in match['alliances']['blue']['teams']:
            team_number = int(team_string[3:])
            team_score = get_score(match, 'blue', team_number)
            if team_score != -1:
                score[team_index_map[team_number]] = score[team_index_map[team_number]] + team_score
                num_matches[team_index_map[team_number]] = num_matches[team_index_map[team_number]] + 1

averages = np.divide(score.astype(float), num_matches.astype(float))
averages = map(lambda x: 0.0 if np.isnan(x[0]) or np.isinf(x[0]) else x[0], averages)

sorted_averages = sorted(zip(sorted_team_list, averages), key=lambda x: x[1], reverse=True)

if options.filename is not None:
    np.savetxt(options.filename, sorted_averages, delimiter=",", fmt='%.3f')
else:
    print sorted_averages
