#!/usr/bin/python

import Blue_Alliance_API
import numpy as np
from os import listdir
from os.path import isfile, join

np.set_printoptions(threshold=np.nan)

interesting_files = [[f, join('events/', f)] for f in listdir('events/') if isfile(join('events/', f))]

for event_code, file_name in interesting_files:
    print 'Looking at %s.' % event_code
    event = Blue_Alliance_API.load_database(file_name)

    # First generate list of all teamnumbers we need to assign each to a index
    sorted_team_list = sorted(map(lambda x: int(x), event.rankings[:,1]))
    team_index_map = dict(zip(sorted_team_list, range(len(sorted_team_list))))

    # Now iterate over the matches and populate what we want.
    matches = [x for x in event.raw_matches if x['comp_level']=='qm']

    print 'Computing for %d matches.' % len(matches)
    score = np.zeros((len(sorted_team_list), 1), np.int32)
    match_matrix = np.zeros((len(sorted_team_list), len(sorted_team_list)), np.int32)

    for match in matches:
        match_num = match['match_number'] - 1

        red_score = match['alliances']['red']['score']
        for team_string in match['alliances']['red']['teams']:
            team_number = int(team_string[3:])
            for team_string_2 in match['alliances']['red']['teams']:
                team_number_2 = int(team_string_2[3:])
                match_matrix[team_index_map[team_number], team_index_map[team_number_2]] = match_matrix[team_index_map[team_number], team_index_map[team_number_2]] + 1
            score[team_index_map[team_number]] = score[team_index_map[team_number]] + red_score

        # Fill in for blue, this is row two
        blue_score = match['alliances']['blue']['score']
        for team_string in match['alliances']['blue']['teams']:
            team_number = int(team_string[3:])
            for team_string_2 in match['alliances']['blue']['teams']:
                team_number_2 = int(team_string_2[3:])
                match_matrix[team_index_map[team_number], team_index_map[team_number_2]] =  match_matrix[team_index_map[team_number], team_index_map[team_number_2]] + 1 
            score[team_index_map[team_number]] = score[team_index_map[team_number]] + blue_score

    # Results
    result = np.dot(np.linalg.inv(match_matrix), score)
    print sorted(zip(sorted_team_list, map(lambda x: x[0], result.tolist())), key=lambda x: x[1], reverse=True)

    break
