#!/usr/bin/python

import csv
import numpy as np
from os import listdir
from os.path import isfile, join
from optparse import OptionParser
from sets import Set

import os

parser = OptionParser()

(options, args) = parser.parse_args()

np.set_printoptions(threshold=np.nan)

interesting_files = [[f, join('divisions/', f)] for f in listdir('divisions/') if isfile(join('divisions/', f))]
for f, filename in interesting_files:
  team_set = Set()
  print 'Opening %s.' % f
  reader=csv.reader(open(filename,"rb"),delimiter=' ')
  teams = map(lambda x: x[0], list(reader))
  print 'Found %d teams.' % len(teams)
  team_set.update(teams)
  team_set = Set(map(lambda x: int(x), team_set))
  print 'Total %d teams.' % len(team_set)

  scores = [[a, join('global_stats/', a)] for a in listdir('global_stats/') if isfile(join('global_stats/', a))]
  print 'Found %d scores.' % len(scores)
  for score_name,score_filename in scores:
    # Read in numpy array from csv in.
    team_rankings = np.genfromtxt(score_filename, delimiter=',')

    # And filter
    if team_rankings[0][0] == 0:
      filtered_teams = filter(lambda x: x[1] in team_set, team_rankings)
      sorted_filtered_teams = [x[1:] for x in filtered_teams]
    else:
      filtered_teams = filter(lambda x: x[0] in team_set, team_rankings)
      sorted_filtered_teams = filtered_teams
    sorted_filtered_teams = np.insert(sorted_filtered_teams, 0, range(len(filtered_teams)), 1)
    np.savetxt('division_stats/%s_%s' % (score_name, f), sorted_filtered_teams, delimiter=",", fmt='%.3f')
