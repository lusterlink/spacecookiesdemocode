#!/usr/bin/python

import Blue_Alliance_API
import numpy as np
import os
from os import listdir
from os.path import isfile, join

np.set_printoptions(threshold=np.nan)

interesting_files = [[f, join('events/', f)] for f in listdir('events/') if isfile(join('events/', f))]

PREFIX='standard_score_opr'

for event_code, file_name in interesting_files:
    print 'Looking at %s.' % event_code

    if os.path.isfile('%s/%s' % (PREFIX, event_code)):
        print 'Skipping %s since have on disk.' % event_code
        continue

    event = Blue_Alliance_API.load_database(file_name)

    # First generate list of all teamnumbers we need to assign each to a index
    sorted_team_list = sorted(map(lambda x: int(x), event.rankings[:,1]))
    team_index_map = dict(zip(sorted_team_list, range(len(sorted_team_list))))

    # Now iterate over the matches and populate what we want.
    qm_matches = [x for x in event.raw_matches if x['comp_level']=='qm']
    if False:
        qf_matches = [x for x in event.raw_matches if x['comp_level']=='qf']
        sf_matches = [x for x in event.raw_matches if x['comp_level']=='sf']
        f_matches = [x for x in event.raw_matches if x['comp_level']=='f']
        ef_matches = [x for x in event.raw_matches if x['comp_level']=='ef']
    else:
        qf_matches = []
        sf_matches = []
        f_matches = []
        ef_matches = []

    if event_code == '2016arlr':
        qm_matches = [x for x in qm_matches if x['match_number']<70]

    num_ef = 24
    num_qf = 12
    num_sf = 6
    num_f = 3
    num_matches = len(qm_matches) + num_qf + num_sf + num_f + num_ef

    print 'Computing for %d matches.' % num_matches
    score = np.zeros((num_matches*2, 1), np.int32)
    match_matrix = np.zeros((num_matches*2, len(sorted_team_list)), np.int32)
    
    for match_type, matches in [('qm', qm_matches), ('qf', qf_matches),
                                ('sf', sf_matches), ('f', f_matches),
                                ('ef', ef_matches)]:
        for match in matches:
            if match_type == 'qm':
                match_num = match['match_number'] - 1
            elif match_type == 'qf':
                match_num = len(qm_matches) + (match['set_number']-1)*3 + match['match_number'] - 1
            elif match_type == 'sf':
                match_num = len(qm_matches) + num_qf + (match['set_number']-1)*3 + match['match_number'] - 1
            elif match_type == 'f':
                match_num = len(qm_matches) + num_qf + num_sf + match['match_number'] - 1
            elif match_type == 'ef':
                match_num = len(qm_matches) + num_qf + num_sf + num_f + (match['set_number']-1)*3 + match['match_number'] - 1

            # Fill in for red, this is row one
            for team_string in match['alliances']['red']['teams']:
                team_number = int(team_string[3:])
                match_matrix[2*match_num, team_index_map[team_number]] = 1

            score[2*match_num] = match['alliances']['red']['score']

            # Fill in for blue, this is row two
            for team_string in match['alliances']['blue']['teams']:
                team_number = int(team_string[3:])
                match_matrix[2*match_num+1, team_index_map[team_number]] = 1
            score[2*match_num+1] = match['alliances']['blue']['score']

    # Results
    results = np.dot(np.dot(np.linalg.inv(np.dot(match_matrix.T, match_matrix)), match_matrix.T), score)

    sorted_associated_results=sorted(zip(sorted_team_list, map(lambda x: x[0], results.tolist())), key=lambda x: x[1], reverse=True)
    np.savetxt('%s/%s' % (PREFIX, event_code), sorted_associated_results, delimiter=",", fmt='%.3f')
