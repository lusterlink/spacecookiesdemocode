#!/usr/bin/python

import Blue_Alliance_API
import numpy as np
import os
from os import listdir
from os.path import isfile, join
from sets import Set
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
        help="write result to FILE", metavar="FILE")
parser.add_option("-r", "--recompute", dest="recompute",
        help="recompute results", default=False)
parser.add_option("-s", "--score", dest="score",
        help="type of score (defense)", default='raw_score')

(options, args) = parser.parse_args()

if options.recompute:
    print 'Forcing recompute'
elif options.filename is not None and os.path.isfile(options.filename):
    print 'Skipping %s since have on disk.' % options.filename
    exit()

def get_histogram_size():
    return 3

def get_scale_faces(match, alliance):
    result = np.zeros(get_histogram_size(), np.int32)
    for i,e in enumerate(['A', 'B', 'C']):
        if match['score_breakdown'][alliance]['towerFace%s' % e] == 'Scaled':
            result[i] = 1
    return result

def get_challenge_faces(match, alliance):
    result = np.zeros(get_histogram_size(), np.int32)
    for i,e in enumerate(['A', 'B', 'C']):
        if match['score_breakdown'][alliance]['towerFace%s' % e] == 'Challenged':
            result[i] = 1
    return result

def get_score(match, alliance, team_number):
    try:
        if options.score == 'scale_faces':
            return get_scale_faces(match, alliance)
        elif options.score == 'challenge_faces':
            return get_challenge_faces(match, alliance)
    except TypeError:
        print match
        return NoneType


np.set_printoptions(threshold=np.nan)
interesting_files = [[f, join('events/', f)] for f in listdir('events/') if isfile(join('events/', f))]

team_numbers_set = Set([])
events = {}
qm_matches = {}

num_matches = 0

print 'Loading data.'

# First load all events and populate set of team numbers
for event_code, file_name in interesting_files:
    #if event_code != '2016casj' and event_code != '2016cada' and event_code != '2016txho':
    #    continue
    events[event_code] = Blue_Alliance_API.load_database(file_name)

    team_numbers_set.update(map(lambda x: int(x), events[event_code].rankings[:,1]))

    qm_matches[event_code] = [x for x in events[event_code].raw_matches if x['comp_level'] == 'qm' and x['alliances']['red']['score'] != -1]

    num_matches = num_matches + len(qm_matches[event_code])

# Now sort
sorted_team_list = sorted([x for x in team_numbers_set])
team_index_map = dict(zip(sorted_team_list, range(len(sorted_team_list))))

score = np.zeros((len(sorted_team_list), get_histogram_size()), np.int32)
num_matches = np.zeros((len(sorted_team_list), 1), np.int32)

for event_code in qm_matches:
    for match in qm_matches[event_code]:
        for team_string in match['alliances']['red']['teams']:
            team_number = int(team_string[3:])
            team_score = get_score(match, 'red', team_number)
            score[team_index_map[team_number]] = np.add(score[team_index_map[team_number]], team_score)
            num_matches[team_index_map[team_number]] = num_matches[team_index_map[team_number]] + 1

        for team_string in match['alliances']['blue']['teams']:
            team_number = int(team_string[3:])
            team_score = get_score(match, 'blue', team_number)
            score[team_index_map[team_number]] = np.add(score[team_index_map[team_number]], team_score)
            num_matches[team_index_map[team_number]] = num_matches[team_index_map[team_number]] + 1

averages = np.divide(score.astype(float), num_matches).tolist()

sorted_averages = zip(range(len(sorted_team_list)), sorted(zip(sorted_team_list, averages), key=lambda x: np.sum(x[1]), reverse=True))
sorted_averages = map(lambda x: [x[0], x[1][0]] + x[1][1], sorted_averages)

if options.filename is not None:
    np.savetxt(options.filename, sorted_averages, delimiter=",", fmt='%.3f')
else:
    print sorted_averages
