#!/usr/bin/python

import Blue_Alliance_API
import time
import os.path

start_time = time.time()
events = Blue_Alliance_API.get_events_and_codes(2016)

for event_long_name, event_code in events:
    print 'Grabbing %s(%s).' % (event_long_name, event_code)

    if os.path.isfile('events/%s' % event_code):
        print 'Skipping %s since have on disk' % event_long_name
        continue

    if event_long_name in ['Archimedes', 'Carver', 'Carson', 'Curie', 'Galileo',
            'Hopper', 'Newton', 'Tesla', 'Einstein', 'Spring Scrimmage', 'Suffield',
            'TNT Week Zero', 'Week 0']:
        print 'Skipping championship division %s.' % event_long_name
        continue

    event = Blue_Alliance_API.Event('team1868', 'testing', '1.0', event_code)
    event.save_database('events/%s' % event_code)

elapsed_time = time.time() - start_time
print 'Took %s seconds.' % str(elapsed_time)
