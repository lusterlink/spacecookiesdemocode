#include <assert.h>
#include <iostream>
#include <math.h>
#include <stdio.h>

#include <algorithm>
#include <string>

#include <opencv2/opencv.hpp>

#define BOARD_WIDTH 9
#define BOARD_HEIGHT 6
#define SQUARE_SIZE (15.0/16.0)

namespace {
void print_usage(const char* name) {
  printf("%s: INPUT_IMAGES CAMERA_CALIB\n", name);
  printf("INPUT_IMAGES: XML file with images.\n");
  printf("CAMERA_CALIB: XML file with camera calibration matrices.\n");
}

}  // namespace

void project_points(const std::vector<cv::Point3f>& object_points,
    const cv::Mat& rotation_mat, const cv::Mat& tvec, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, std::vector<cv::Point2f>* image_points);

void levenberg_marq(const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, cv::Mat* tvec, cv::Mat* rvec);

int main(int argc, char** argv) {
  if (argc != 3) {
    print_usage(argv[0]);
    return -1;
  }

  std::string input_xml_filename(argv[1]);

  printf("Reading images from xml: %s\n", input_xml_filename.c_str());

  cv::FileStorage fs(input_xml_filename, cv::FileStorage::READ);
  // Read in image filenames.
  cv::FileNode fn = fs["calib_images"];
  if (fn.type() != cv::FileNode::SEQ) {
    fprintf(stderr, "calib_image is a not a sequence in xml!!\n");
    return -1;
  }

  // Open XML
  std::vector<std::string> image_filenames;
  for (cv::FileNodeIterator it = fn.begin(); it != fn.end(); ++it) {
    image_filenames.push_back(*it);
  }

  // Be verbose about what we read in.
  printf("Reading ");
  for (const std::string& filename : image_filenames) {
    printf("%s, ", filename.c_str());
  }
  printf("\n");

  // Read in all images
  std::vector<cv::Mat> input_images;
  for (const std::string& filename : image_filenames) {
    // Read grayscale images in on purpose.
    input_images.push_back(cv::imread(filename));
  }

  // Read in camera matrices
  std::string camera_xml_filename(argv[2]);
  printf("Reading camera matrices from %s.\n", camera_xml_filename.c_str());

  cv::FileStorage fs_camera(camera_xml_filename, cv::FileStorage::READ);
  cv::Mat cameraMatrix, distCoef;
  fs_camera["cameraMatrix"] >> cameraMatrix;
  fs_camera["distCoef"] >> distCoef;

  // This is vector of the coordinates of the interior corners of a chessboard
  // ordered row-major.
  std::vector<cv::Point3f> chessboard_corners;
  for (int i = 0; i < BOARD_HEIGHT; ++i) {
    for (int j = 0; j < BOARD_WIDTH; ++j) {
      chessboard_corners.push_back(
          cv::Point3f(j * SQUARE_SIZE, i * SQUARE_SIZE, 0));
    }
  }

  // Set up the outer of the inner corners for redrawing.
  std::vector<cv::Point3f> chessboard_outer_corners;
  chessboard_outer_corners.push_back({0, 0, 0});
  chessboard_outer_corners.push_back({SQUARE_SIZE * (BOARD_WIDTH - 1), 0, 0});
  chessboard_outer_corners.push_back(
      {SQUARE_SIZE * (BOARD_WIDTH - 1), SQUARE_SIZE * (BOARD_HEIGHT - 1), 0});
  chessboard_outer_corners.push_back({0, SQUARE_SIZE * (BOARD_HEIGHT - 1), 0});

  cv::namedWindow("Results");
  cv::Mat output_chessboard_drawn;
  cv::Mat image;
  cv::VideoCapture cap(0);
  
  cv::Mat frame;
  if (!cap.isOpened()) {
    fprintf(stderr, "Unable to open camera!\n");
    return -1;
  }
  while (true) {
    cap >> frame;
    cvtColor(frame, image, CV_RGB2GRAY);

    std::vector<cv::Point2f> single_image_corners;
    bool found =
      cv::findChessboardCorners(image, cv::Size(BOARD_WIDTH, BOARD_HEIGHT),
          single_image_corners);

    frame.copyTo(output_chessboard_drawn);
    if (found) {
      cv::cornerSubPix(image, single_image_corners, cv::Size(3, 3),
          cv::Size(-1, -1), cv::TermCriteria(cv::TermCriteria::COUNT +
            cv::TermCriteria::EPS, 30, 0.01));
    } else {
      printf("Not found!\n");
      cv::waitKey(10);
      continue;
    }

    //cv::drawChessboardCorners(output_chessboard_drawn,
    //   cv::Size(BOARD_WIDTH, BOARD_HEIGHT), single_image_corners, found);

    // Call SolvePnp
    cv::Mat rvec, tvec, rotation_mat;
    std::vector<cv::Point2f> reprojected_points;
    //bool solve_success = cv::solvePnP(chessboard_corners, single_image_corners,
     //   cameraMatrix, distCoef, rvec, tvec);
    levenberg_marq(chessboard_corners, single_image_corners, cameraMatrix,
        distCoef, &tvec, &rvec);

    if (true) {
      cv::Rodrigues(rvec, rotation_mat);
      project_points(chessboard_outer_corners, rotation_mat, tvec, cameraMatrix,
          distCoef, &reprojected_points);

      //cv::projectPoints(chessboard_outer_corners, rvec, tvec, cameraMatrix,
      //    distCoef, reprojected_points);

      // Now draw 4 lines. On the input_images.
      for (size_t j = 0; j < reprojected_points.size() - 1; ++j) {
        cv::line(output_chessboard_drawn, reprojected_points[j],
            reprojected_points[j+1], CV_RGB(255, 0, 0), 5);
      }
      cv::line(output_chessboard_drawn,
          reprojected_points[reprojected_points.size() - 1],
          reprojected_points[0], CV_RGB(255, 0, 0), 5);

      imshow("Results", output_chessboard_drawn);
    }

   int key_code = cv::waitKey(10);

   if (key_code == 'q') {
      // If 'q', quit.
      break;
   }
  }
}

// Fill in the jacobian col in the jacobian for a given set of correspondences.
void fillJacobianCol(cv::Mat* jacobian,
    const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points,
    const cv::Mat& residuals,
    const cv::Mat& tvec, const cv::Mat& rvec, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, int col_num, double step) {
  std::vector<cv::Point2f> image_points_projected;
  cv::Mat rotation_mat;
  cv::Rodrigues(rvec, rotation_mat);

  assert(object_points.size() == image_points.size());

  // Project points for this current step.
  //cv::projectPoints(object_points, rvec, tvec, cameraMatrix, distCoef,
   //   image_points_projected);
  project_points(object_points, rotation_mat, tvec, cameraMatrix, distCoef,
      &image_points_projected);

  // Now update Jacobian.
  for (size_t i = 0; i < object_points.size(); ++i) {
    /*std::cout << "Residual: " << residuals.at<double>(2*i, 0) << std::endl;
    std::cout << "project: " << image_points_projected[i].x  << std::endl;
    std::cout << "image_point: " << image_points[i].x << std::endl;
    std::cout << "new residaul: " << (image_points_projected[i].x - image_points[i].x) << std::endl;*/
    jacobian->at<double>(2*i, col_num) =
      ((image_points_projected[i].x - image_points[i].x) - residuals.at<double>(2*i, 0)) *  (1.0/step);
    jacobian->at<double>(2*i+1, col_num) =
      ((image_points_projected[i].y - image_points[i].y) - residuals.at<double>(2*i+1, 0)) * (1.0/step);
  }
}

void computeJacobian(cv::Mat* jacobian,
    const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points,
    const cv::Mat& residuals, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, const cv::Mat& tvec, const cv::Mat& rvec) {
  // We shall let guess be {rvec, tvec} where rvec is a Rodrigues.
  const double step = 1e-6;

  assert(object_points.size() == image_points.size());
  assert(tvec.rows == 3);
  assert(tvec.cols == 1);
  assert(rvec.rows == 3);
  assert(rvec.cols == 1);

  // Each row of the Jacobian is a Jacobian row vector.
  *jacobian = cv::Mat::zeros(object_points.size() * 2, 6, CV_64F);

  cv::Mat tvec_h = tvec;
  cv::Mat rvec_h = rvec;

  // Step tvec(0, 0).
  tvec_h.at<double>(0, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 0, step);
  tvec_h.at<double>(0, 0) -= step;
 

  // Step tvec(1, 0)
  tvec_h.at<double>(1, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 1, step);
  tvec_h.at<double>(1, 0) -= step;

  // Step tvec(2, 0)
  tvec_h.at<double>(2, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 2, step);
  tvec_h.at<double>(2, 0) -= step;
 
  // Step rvec(0, 0)
  rvec_h.at<double>(0, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 3, step);
  rvec_h.at<double>(0, 0) -= step;

  // Step rvec(1, 0)
  rvec_h.at<double>(1, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 4, step);
  rvec_h.at<double>(1, 0) -= step;

  // Step rvec(2, 0)
  rvec_h.at<double>(2, 0) += step;
  fillJacobianCol(jacobian, object_points, image_points, residuals, tvec_h, rvec_h,
      cameraMatrix, distCoef, 5, step);
  rvec_h.at<double>(2, 0) -= step;
}

double compute_residuals(const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, const cv::Mat& tvec, const cv::Mat& rvec,
    cv::Mat* residuals) {
  assert(residuals->rows == object_points.size() * 2);

  double current_error = 0.0;

  // Compute the residuals
  std::vector<cv::Point2f> image_points_projected;
  cv::Mat rotation_mat;
  cv::Rodrigues(rvec, rotation_mat);

  project_points(object_points, rotation_mat, tvec, cameraMatrix, distCoef,
        &image_points_projected);
   
  for (size_t i = 0; i < object_points.size(); ++i) {
    double x_error = image_points_projected[i].x - image_points[i].x;
    double y_error = image_points_projected[i].y - image_points[i].y;
    residuals->at<double>(2*i, 0) = x_error;
    residuals->at<double>(2*i+1, 0) = y_error;
    current_error += x_error * x_error;
    current_error += y_error * y_error;
  }
  current_error = sqrt(current_error);
  return current_error;
}

double levenberg_marq_iteration(const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, cv::Mat* tvec, cv::Mat* rvec) {
  const int kMaxIterations = 200;
  const double kResidualSlopeEpi = 1e-8;
  const double kStepEpi = 1e-8;

  int damping_scale_factor = 2;
  double damping_init_scale = 1e-8;
  double damping_factor, current_error;
  cv::Mat jacobian, residuals, jacobian_square, residual_slope, step,
    damping_mat, current_guess, next_guess, next_tvec, next_rvec,
    next_residuals, gain_denom;
  residuals = cv::Mat::zeros(object_points.size() * 2, 1, CV_64F);
  next_residuals = cv::Mat::zeros(object_points.size() * 2, 1, CV_64F);

  // Compute the residuals for the first Jacobian.
  current_error = compute_residuals(
      object_points, image_points, cameraMatrix, distCoef, *tvec, *rvec,
      &residuals);

  // Compute the first Jacobian for the starting damping value.
  computeJacobian(&jacobian, object_points, image_points, residuals,
      cameraMatrix, distCoef, *tvec, *rvec);

  jacobian_square = jacobian.t() * jacobian;

  // Compute the residual slope.
  residual_slope = jacobian.t() * residuals;

  // Find the max value on the diagonal.
  damping_factor = jacobian_square.at<double>(0, 0);
  for (int i = 1; i < jacobian_square.rows; ++i) {
    damping_factor = std::max(jacobian_square.at<double>(i, i), damping_factor);
  }
  damping_factor *= damping_init_scale;

  // Compute first guess.
  current_guess = (cv::Mat_<double>(6,1) << tvec->at<double>(0, 0),
      tvec->at<double>(1, 0), tvec->at<double>(2, 0),
      rvec->at<double>(0, 0), rvec->at<double>(1, 0),
      rvec->at<double>(2, 0));

  for (int k = 0; k < kMaxIterations; ++k) {
    // Check if we are done if the residual_slope is close to 0.
    if (cv::norm(residual_slope, cv::NORM_INF) < kResidualSlopeEpi) {
      return current_error; 
    }

    // Find next step.
    damping_mat = cv::Mat::eye(6, 6, CV_64F) * damping_factor;
    step = (jacobian_square + damping_mat).inv() * residual_slope;
    step = -step;

    // Check if our step is too small.
    if (cv::norm(step) < kStepEpi* (cv::norm(current_guess) + kStepEpi)) {
      return current_error;
    }

    // Otherwise, check properties of next step.
    next_guess = current_guess + step;

    next_tvec = (cv::Mat_<double>(3, 1) << next_guess.at<double>(0, 0),
        next_guess.at<double>(1, 0), next_guess.at<double>(2, 0));
    next_rvec = (cv::Mat_<double>(3, 1) << next_guess.at<double>(3, 0),
        next_guess.at<double>(4, 0), next_guess.at<double>(5, 0));

    double next_error = compute_residuals(object_points, image_points,
        cameraMatrix, distCoef, next_tvec, next_rvec, &next_residuals);

    gain_denom = 0.5 * step.t() * (damping_factor * step - residual_slope);
    assert (gain_denom.at<double>(0, 0) > 0);
    double gain_ratio =
      (current_error - next_error) / gain_denom.at<double>(0, 0);
    if (gain_ratio) {
      // Good gain ratio.  Step entire guess.
      current_guess = next_guess;
      residuals = next_residuals;
      *tvec = next_tvec;
      *rvec = next_rvec;
      current_error = next_error;

      computeJacobian(&jacobian, object_points, image_points, residuals,
          cameraMatrix, distCoef, *tvec, *rvec);
      jacobian_square = jacobian.t() * jacobian;
      residual_slope = jacobian.t() * residuals;

      // Update damping factor.
      damping_factor = damping_factor * std::min(1.0/3.0,
          1 - pow(2.0 * gain_ratio - 1, 3.0));
      damping_scale_factor = 2;
    } else {
      damping_factor *= damping_scale_factor;
      damping_scale_factor *= 2;
    }
  }

  return current_error;
}


void levenberg_marq(const std::vector<cv::Point3f>& object_points,
    const std::vector<cv::Point2f>& image_points, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, cv::Mat* tvec, cv::Mat* rvec) {
  double min_error = std::numeric_limits<double>::max();
  cv::Mat test_tvec, test_rvec;

  for (int i = 0; i < 1; ++i) {
    for (int j = 0; j < 1; ++j) {
      for (int k = 0; k < 1; ++k) {

        test_tvec = (cv::Mat_<double>(3, 1) << 1,1,10);
        test_rvec = (cv::Mat_<double>(3, 1) << 0 + i*2, 0 + j*2, 0+ k*2);
  
        double new_error =
          levenberg_marq_iteration(object_points, image_points, cameraMatrix,
              distCoef, &test_tvec, &test_rvec);
        if (new_error < min_error) {
          min_error = new_error;
          *tvec = test_tvec;
          *rvec = test_rvec;
        }
      }
    }
  }
  std::cout << "Final error: " << min_error << std::endl;
  return;
}

void project_points(const std::vector<cv::Point3f>& object_points,
    const cv::Mat& rotation_mat, const cv::Mat& tvec, const cv::Mat& cameraMatrix,
    const cv::Mat& distCoef, std::vector<cv::Point2f>* image_points) {
  assert(image_points != nullptr);
  image_points->clear();

  cv::Mat object_point_mat, after_rotation, after_translation;

  // Store distortion coefficients for radial and tangentail correction.
  double k1 = distCoef.at<double>(0, 0);
  double k2 = distCoef.at<double>(1, 0);
  double p1 = distCoef.at<double>(2, 0);
  double p2 = distCoef.at<double>(3, 0);

  // Store projection constant.
  double fx = cameraMatrix.at<double>(0, 0);
  double fy = cameraMatrix.at<double>(1, 1);
  double cx = cameraMatrix.at<double>(0, 2);
  double cy = cameraMatrix.at<double>(1, 2);

  for (const cv::Point3f& object_point : object_points) {
    object_point_mat = (cv::Mat_<double>(3, 1) << object_point.x,
        object_point.y, object_point.z);

    // First apply rotation.
    after_rotation = rotation_mat * object_point_mat;

    // Next translation
    after_translation = after_rotation + tvec;

    // Now project to image plane.
    double z = after_translation.at<double>(2, 0) ?
      1./after_translation.at<double>(2, 0) : 1;
    double x = after_translation.at<double>(0, 0) * z;
    double y = after_translation.at<double>(1, 0) * z;

    // Apply distortion model.
    double x_s = x * x;
    double y_s = y * y;
    double r2 = x_s + y_s;
    double r4 = r2 * r2;

    double x2 = x * (1 + k1*r2 + k2*r4) + 2*p1*x*y + p2*(r2 + 2*x_s);
    double y2 = y * (1 + k1*r2 + k2*r4) + p1*(r2 + 2*y_s) + 2*p2*x*y; 

    // Finally project to image plane
    double u = fx * x2 + cx;
    double v = fy * y2 + cy;
    image_points->emplace_back(u, v);
  }
}
